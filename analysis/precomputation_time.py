from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm


def print_mat(mat, name, log=False):

    print()
    print()

    for i in mat:
        print(i)

    fig, ax = plt.subplots()

    if log == True:
        ims = ax.imshow(mat, origin="lower", norm=LogNorm())
    else:
        ims = ax.imshow(mat, origin="lower")

    plt.colorbar(ims)

    plt.xlabel("$\lambda$")
    plt.ylabel("$w$")

    rangge = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    plt.xticks(rangge, ["$2^{" + str(k + 4) + "}$" for k in rangge])
    plt.yticks([0, 1, 2, 3, 4, 5, 6], [2, 3, 4, 5, 6, 7, 8])
    plt.savefig(name)
    plt.cla()


def process_file(l, k):
    file_name = "results/" + str(l) + "_" + str(k) + ".csv"

    f = open(file_name)
    pre = []
    tot = []
    perc = []

    for l in f:
        arr = l.split(';')
        p = float(arr[0])
        t = float(arr[1])

        pre.append(p)
        tot.append(t)
        perc.append(p / t * 100)

    return [np.mean(pre), np.mean(tot), np.mean(perc)]


min_l = 4
max_l = 15

min_w = 2
max_w = 8

range_w = [2, 3, 4, 5, 6, 7, 8]
range_l = range(min_l, max_l)


mat_precomp = [[None for i in range_l] for j in range_w]
mat_total = [[None for i in range_l] for j in range_w]
mat_percentage = [[None for i in range_l] for j in range_w]


for ll in range_l:
    print(ll)
    for w in range_w:
        print("\t" + str(w))
        l = 2**ll
        values = process_file(l, w)

        mat_precomp[w - min_w][ll - min_l] = values[0]
        mat_total[w - min_w][ll - min_l] = values[1]
        mat_percentage[w - min_w][ll - min_l] = values[2]


print_mat(mat_percentage, "precomputation_percentage.pdf", log=True)
print_mat(mat_total, "precomputation_total_time.pdf", log=True)
print_mat(mat_precomp, "precomputation_time.pdf", log=True)


'''
for i in best_algo:
    print(i)

cmap = colors.ListedColormap(['red', 'limegreen', 'darkgreen',
                              'midnightblue', 'mediumblue', 'dodgerblue', 'lightskyblue'])
bounds = [1, 2, 3, 4, 5, 6, 7, 8]
norm = colors.BoundaryNorm(bounds, cmap.N)

fig, ax = plt.subplots()
img = ax.imshow(best_algo, cmap=cmap, norm=norm, origin="lower")
cbar = plt.colorbar(img, ticks=[1, 2, 3, 4, 5, 6, 7, 8, 9])
cbar.set_ticks([])

x = 10

cbar.ax.text(x, 1.40, "Naive")
cbar.ax.text(x, 2.40, "Naive\nParallel")
cbar.ax.text(x, 3.40, "Optimized\nParallel")
cbar.ax.text(x, 4.40, "Lenstra2")
cbar.ax.text(x, 5.40, "Lenstra3")
cbar.ax.text(x, 6.40, "Lenstra4")
cbar.ax.text(x, 7.40, "Lenstra5")


ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=1)

plt.xticks(np.arange(-0.5, max_k - min_k, 1))
ax.set_xticklabels('')
ax.set_xticks(np.arange(0, max_k - min_k, 1), minor=True)
ax.set_xticklabels(["$2^{" + str(k) + "}$" for k in range_k], minor=True)


plt.yticks(np.arange(-0.5, max_l - min_l, 1))
ax.set_yticklabels('')
ax.set_yticks(np.arange(0, max_l - min_l, 1), minor=True)
ax.set_yticklabels(["$2^{" + str(l) + "}$" for l in range_l], minor=True)

ax.tick_params(axis='both', which='both', length=0)

plt.xlabel("Value of $k$")
plt.ylabel("Value of $\lambda$")

plt.savefig("best_algo.pdf")
'''
