from matplotlib import colors
import matplotlib.pyplot as plt
import numpy as np


def process_file(l, k):
    file_name = "results/" + str(l) + "_" + str(k) + ".csv"

    f = open(file_name)
    mat = np.loadtxt(open(file_name, "r"), delimiter=";")
    return list(map(np.mean, list(mat.T)))


l = 4096

naive = []
naivePar = []
optiPar = []
lenstra2 = []
lenstra3 = []
lenstra4 = []
lenstra5 = []

min_k = 4
max_k = 14

min_l = 4
max_l = 15

range_k = range(min_k, max_k)
range_l = range(min_l, max_l)

best_algo = [[0 for i in range_k] for i in range_l]
code_colors = {
    "naive": 1,
    "naivePar": 2,
    "optiPar": 3,
    "lenstra2": 4,
    "lenstra3": 5,
    "lenstra4": 6,
    "lenstra5": 7,
    "arry2": 8,
    "arry3": 9,
    "arry4": 10,
    "arry5": 11,
}


for ll in range_l:
    for kk in range_k:
        k = 2**kk
        l = 2**ll
        values = process_file(l, k)

        base = values[0]

        dico = {
            "naive": base / values[0],
            # "naivePar": base / values[1],
            # "optiPar": base / values[2],
            "lenstra2": base / (values[4]),
            "lenstra3": base / (values[6]),
            "lenstra4": base / (values[8]),
            "lenstra5": base / (values[10]),
            "arry2": base / (values[12]),
            "arry3": base / (values[14]),
            "arry4": base / (values[16]),
            "arry5": base / (values[18]),
        }

        # print(dico)
        # print(min(dico, key=dico.get))
        best_algo[ll - min_l][kk -
                              min_k] = code_colors[max(dico, key=dico.get)]
        print("\t" + str(ll - 4) + "\t" + str(kk - 2) + "\t" +
              str(code_colors[max(dico, key=dico.get)]))
        print(dico)

for i in best_algo:
    print(i)

cmap = colors.ListedColormap(['red',
                              'limegreen', 'darkgreen',
                              'midnightblue', 'mediumblue', 'dodgerblue', 'lightskyblue',
                              'magenta', 'orchid', 'darkorchid', 'purple'])
bounds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
norm = colors.BoundaryNorm(bounds, cmap.N)

fig, ax = plt.subplots()
img = ax.imshow(best_algo, cmap=cmap, norm=norm, origin="lower")
cbar = plt.colorbar(img, ticks=[1, 2, 3, 4, 5, 6, 7, 8, 9])
cbar.set_ticks([])

x = 15

cbar.ax.text(x, 1.40, "Naive")
cbar.ax.text(x, 2, "Naive\nParallel")
cbar.ax.text(x, 3, "Optimized\nParallel")
cbar.ax.text(x, 4.40, "Lenstra2")
cbar.ax.text(x, 5.40, "Lenstra3")
cbar.ax.text(x, 6.40, "Lenstra4")
cbar.ax.text(x, 7.40, "Lenstra5")
cbar.ax.text(x, 8.40, "$2^2$-arry")
cbar.ax.text(x, 9.40, "$2^3$-arry")
cbar.ax.text(x, 10.40, "$2^4$-arry")
cbar.ax.text(x, 11.40, "$2^5$-arry")


ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=1)

plt.xticks(np.arange(-0.5, max_k - min_k, 1))
ax.set_xticklabels('')
ax.set_xticks(np.arange(0, max_k - min_k, 1), minor=True)
ax.set_xticklabels(["$2^{" + str(k) + "}$" for k in range_k], minor=True)


plt.yticks(np.arange(-0.5, max_l - min_l, 1))
ax.set_yticklabels('')
ax.set_yticks(np.arange(0, max_l - min_l, 1), minor=True)
ax.set_yticklabels(["$2^{" + str(l) + "}$" for l in range_l], minor=True)

ax.tick_params(axis='both', which='both', length=0)

plt.xlabel("Value of $k$")
plt.ylabel("Value of $\lambda$")

plt.savefig("best_algo.pdf")
