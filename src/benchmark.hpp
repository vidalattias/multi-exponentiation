#ifndef BENCHMARK_HPP
#define BENCHMARK_HPP

#include "gmp/gmp.h"
#include <chrono>
typedef void (* vFunctionCall)(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N);

typedef void (* vWindowCall)(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, mpz_t * pc);

typedef void (* vWindowPrecomput)(mpz_t* pc, int pow_w, mpz_t x, mpz_t y, mpz_t N);

/**
 * Takes a multiexponentiation algorithm f in parameter and computes ret = (x^a) * (x^b) mod N using f and prints out the time taken to compute.
 * @method benchmark
 * @param  f         Multiexponentiation algorithm
 * @param  ret       Return variable holder
 * @param  x
 * @param  y
 * @param  a
 * @param  b
 * @param  N         Modulus
 * @param  end       If true then prints a final ";". Should be used only if it is not the last benchmark so the line in a CSV file does not end by a ";" and causes bugs when parsing.
 */
void benchmark (vFunctionCall f, mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, bool end);

/**
 * Takes a multiexponentiation algorithm f of the Lenstra family in parameter and computes ret = (x^a) * (x^b) mod N using f and prints out the time taken to compute.
 * @method benchmark
 * @param  f         Multiexponentiation algorithm
 * @param  ret       Return variable holder
 * @param  x
 * @param  y
 * @param  a
 * @param  b
 * @param  N         Modulus
 * @param  k         Max exponent bitlength
 * @param  w         Lenstra window size
 * @param  end       If true then prints a final ";". Should be used only if it is not the last benchmark so the line in a CSV file does not end by a ";" and causes bugs when parsing.
 */
void benchmark (vWindowCall f, vWindowPrecomput p, mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, bool end);


class Benchmark
{
public:
    std::chrono::duration<double> computation_time;
    virtual void time() = 0;
};


class DirectBenchmark : public Benchmark
{
public:
  std::chrono::duration<double> precomputation_time;
    DirectBenchmark(vWindowCall f, vWindowPrecomput p, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int w);
    void time();
};


class WindowBenchmark : public Benchmark
{
public:
    WindowBenchmark(vFunctionCall f, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int w);
    void time();
};

#endif
