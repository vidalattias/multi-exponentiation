#ifndef LENSTRA_H
#define LENSTRA_H

#include "gmp/gmp.h"

/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */
void lenstra_exponentiation(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, mpz_t * pc);


void lenstra_precomputations(mpz_t* pc, int pow_w, mpz_t x, mpz_t y, mpz_t N);
#endif
