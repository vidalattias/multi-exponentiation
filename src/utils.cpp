#include "utils.hpp"
#include "gmp.h"
#include <cmath>
#include "gmp/gmp-impl.h"


extern mpz_t trash;

/**
 * Returns True if bits at index J of x and y are simultaneously 0, returns False otherwise
 * Used in the Lenstra multiexponentiation to alleviate the code reading.
 *
 * @param  x Exponent 1
 * @param  y Exponent 2
 * @param  j Index to test the bits
 * @return
 */
bool simultaneous_zero_bit(mpz_t x, mpz_t y, int j)
{
        if(mpz_tstbit(x, j) == 0 && mpz_tstbit(y, j) == 0)
        {
                return true;
        }
        else
        {
                return false;
        }
}


/**
 * [filter description]
 * @param  e [description]
 * @param  j [description]
 * @param  J [description]
 * @return
 */
int filter(mpz_t e, int i, int j)
{

        mpz_fdiv_q_2exp(trash, e, j);

        return mpz_fdiv_ui(trash, pow(2, i-j+1));
}


void extended_euclide(mpz_t ret_gcd, mpz_t ret_a, mpz_t ret_b, mpz_t a, mpz_t b)
{

        if(mpz_cmp_ui(b, 0)==0)
        {
                mpz_set(ret_gcd, a);
                mpz_set_ui(ret_a,1);
                mpz_set_ui(ret_b,0);
                return;
        }
        else
        {
                mpz_t tmp_mod;
                mpz_init(tmp_mod);
                mpz_mod(tmp_mod, a, b);
                extended_euclide(ret_gcd, ret_a, ret_b, b, tmp_mod);


                mpz_swap(ret_a, ret_b);
                mpz_fdiv_q(tmp_mod, a, b);
                mpz_mul(tmp_mod, tmp_mod, ret_a);
                mpz_sub(ret_b, ret_b, tmp_mod);
                return;
        }

}

void modular_inverse(mpz_t x, mpz_t a, mpz_t m)
{
        mpz_mod(a, a, m);
        for (mpz_set_ui(x, 1); mpz_cmp(x, m)<0; mpz_add_ui(x, x, 1))
        {
                mpz_t tt;
                mpz_init(tt);
                mpz_mul(tt, a, x);
                mpz_mod(tt, tt, m);
                if (mpz_cmp_ui(tt, 1) == 0)
                {
                        return;
                }
        }
}


void mpnc_shift(mp_ptr rp, mp_size_t rs, int offset)
{

  int words_to_move = offset/64;

  for(int i = 0; i < rs-words_to_move; i++)
  {
    rp[i] = rp[i+words_to_move];
  }
}
