#include "gmp/gmp.h"
#include "gmp/gmp-impl.h"
#include <iostream>
#include <stdlib.h>

#include "montgomery.hpp"
#include "utils.hpp"

extern mp_ptr np, tmpp, tmp2p, npp, n0p, ret_tmp_p;
extern mpz_t tmp, tmp2, N, N_prime, R, R_prime, n, Q, trash, mu, R_and;
extern mp_size_t ns;
int carry, w;
extern int lambda;
extern mp_limb_t np0;
mp_limb_t mulc;


extern mpz_t beta_n, beta_n2, beta_n2_and, mu2, n_prime2;


void redcify(mp_ptr rp, mp_srcptr up, mp_size_t un, mp_srcptr mp, mp_size_t n)
{
        mp_ptr tp, qp;
        TMP_DECL;
        TMP_MARK;

        TMP_ALLOC_LIMBS_2 (tp, un + n, qp, un + 1);

        MPN_ZERO (tp, n);
        MPN_COPY (tp + n, up, un);
        mpn_tdiv_qr (qp, rp, 0L, tp, un + n, mp, n);
        TMP_FREE;
}


void montgomery_mul(mpz_t r, mpz_t x, mpz_t y)
{
        mpz_mul(r, x, y);

        fast_REDC(r, r);
}


// Implementing fast REDC from Algorithm 2.7 in Modern Computer Arithmetic book (Brent & Zimmermann)

inline void fast_REDC(mpz_t r, mpz_t c)
{
        mpz_mul(Q, N_prime, c);
        mpz_and(Q, Q, R_and);

        mpz_mul(trash, Q, N);
        mpz_add(r, c, trash);


        if(SIZ(r) >= lambda/64)
        {
                //mpz_fdiv_q_2exp(r, r, lambda);
                mpnc_shift(PTR(r), SIZ(r), lambda);
                SIZ(r) -= lambda/64;
        }
        else
        {
                mpz_set_ui(r, 0);
        }


        if(mpz_cmp(r, N)>0)
        {
                mpz_sub(r, r, N);
        }
}


inline void REDCSvoboda(mpz_t r, mpz_t c)
{
        //std::cout << "c = " << c << std::endl;
        //std::cout << "N = " << N << std::endl;

        //std::cout << "Mu = " << mu2 << std::endl;

        //std::cout << "N' = " << n_prime2 << std::endl;
        mpz_t q0;
        mpz_init(q0);

        mpz_and(q0, c, beta_n2_and);
        //std::cout << "q0 = " << q0 << std::endl;


        mpz_mul(q0, q0, n_prime2);
        mpz_add(c, c, q0);
        //std::cout << "Before shift : \n\t" << c << std::endl;
        //mpnc_shift(PTR(c), SIZ(c), 256);
        mpz_fdiv_q_2exp(c, c, 256);
        //mpnc_shift(PTR(c), SIZ(c), 512);
        //std::cout << "c = " << c << std::endl;

        mpz_t q1;
        mpz_init(q1);

        mpz_mul(q1, mu2, c);
        mpz_and(q1, q1, beta_n2_and);

        //std::cout << "q1 = " << q1 << std::endl;

        mpz_mul(q1, q1, N);
        mpz_add(r, c, q1);

        //std::cout << "c+q1*N = " << r << std::endl;
        mpz_fdiv_q_2exp(c, c, 256);


        if(mpz_cmp(r, N)>0)
        {
                mpz_sub(r, r, N);
        }
}

int mpn_bit_level_montgomery_mul(mp_ptr rp, mp_ptr xp, mp_size_t xs)
{
        mp_size_t rs = 1;
        mp_size_t tmps = 0;
        mp_size_t tmp2s = 0;
        rp[0] = 0;


        mp_ptr old_rp = 0;

        if(rp == xp)
        {
                old_rp = rp;
                rp = ret_tmp_p;
        }

        for(int i = 0; i < ns; i++)
        {
                /*
                   Computing here u = u + a_i*b
                 */
                // That is u = u + tmp
                //
                carry = mpn_add_1(rp, rp, rs, xp[i]);

                //carry = mpn_add(rp, tmpp, tmps, rp, rs);
                rp[rs] = carry;
                if(carry)
                        rs++;

                /*
                   Computing here u = u + a_i*b
                 */


                // Computing tmp = np0 * rp[0]
                tmpp[0] = rp[0] * np0;
                // End of tmp = np0 * rp[0]

                // Computing tmp2p = tmpp * n
                mulc = mpn_mul_1(tmp2p, np, ns, tmpp[0]);
                tmp2s = ns;
                tmp2p[tmp2s] = mulc;

                if(mulc)
                {
                        tmp2s++;
                }

                carry = mpn_add(rp, tmp2p, tmp2s, rp, rs);

                rs = tmp2s;
                rp[rs] = carry;
                if(carry)
                        rs += carry;
                //mpn_rshift(rp, rp, rs, w);
                for(int i = 0; i < rs-1; i++)
                {
                        rp[i] = rp[i+1];
                }
                rp[rs] = 0;
                rs--;
        }
        if(mpn_cmp(rp, np, rs)>0)
        {
                mpn_sub_n(rp, rp, np, rs);
        }

        if(old_rp == xp)
        {
                rp = old_rp;

                for(int i = 0; i < rs; i++)
                {
                        rp[i] = ret_tmp_p[i];
                }
        }

        return rs;
}


int mpn_word_level_montgomery_mul(mp_ptr rp, mp_ptr ap, mp_size_t as, mp_ptr bp, mp_size_t bs)
{
        mp_size_t rs = 0;
        mp_size_t tmps = 0;
        mp_size_t tmp2s = 0;


        rp[0] = 0;

        for(int i = 0; i < ns; i++)
        {
                /*
                   Computing here u = u + a_i*b
                 */

                // That is tmp = a_i*b

                mulc = mpn_mul_1(tmpp, bp, bs, ap[i]);
                tmps = bs;
                tmpp[tmps] = mulc;

                if(mulc)
                {
                        tmps++;
                }

                // That is u = u + tmp
                carry = mpn_add(rp, tmpp, tmps, rp, rs);

                rs = tmps;
                rp[rs] = carry;
                if(carry)
                        rs += carry;

                /*
                   Computing here u = u + a_i*b
                 */


                // Computing tmp = np0 * rp[0]
                tmpp[0] = rp[0] * np0;
                // End of tmp = np0 * rp[0]

                // Computing tmp2p = tmpp * n
                mulc = mpn_mul_1(tmp2p, np, ns, tmpp[0]);
                tmp2s = ns;
                tmp2p[tmp2s] = mulc;

                if(mulc)
                {
                        tmp2s++;
                }

                carry = mpn_add(rp, tmp2p, tmp2s, rp, rs);

                rs = tmp2s;
                rp[rs] = carry;
                if(carry)
                        rs += carry;
                //mpn_rshift(rp, rp, rs, w);
                for(int i = 0; i < rs-1; i++)
                {
                        rp[i] = rp[i+1];
                }
                rp[rs] = 0;
                rs--;
        }
        if(mpn_cmp(rp, np, rs)>0)
        {
                mpn_sub_n(rp, rp, np, rs);

        }


        return rs;
}
