#include "naive.hpp"

/**
 * Computes ret = (x^a)*(y^b) mod N
 * Does it the naive way : computes sequentially x1 = x^a mod N, then x2 = y^b mod N and finally does ret = (x1 * x2) mod N.
 *
 * @param ret [description]
 * @param x   [description]
 * @param y   [description]
 * @param a   [description]
 * @param b   [description]
 * @param N   [description]
 */

void naive_multiexp(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N)
{
        mpz_t x1, x2;
        mpz_init(x1);
        mpz_init(x2);

        mpz_powm(x1, x, a, N);
        mpz_powm(x2, y, b, N);

        mpz_mul(ret, x1, x2);
        mpz_mod(ret, ret, N);
}
