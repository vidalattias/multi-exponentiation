#ifndef UTILS_H
#define UTILS_H

#include "gmp.h"

#include <iostream>
/**
 * Returns True if bits at index J of x and y are simultaneously 0, returns False otherwise
 * Used in the Lenstra multiexponentiation to alleviate the code reading.
 *
 * @param  x Exponent 1
 * @param  y Exponent 2
 * @param  j Index to test the bits
 * @return
 */
bool simultaneous_zero_bit(mpz_t x, mpz_t y, int j);

/**
 * [filter description]
 * @param  e [description]
 * @param  j [description]
 * @param  J [description]
 * @return
 */
int filter(mpz_t e, int i, int j);


void extended_euclide(mpz_t ret_gcd, mpz_t ret_a, mpz_t ret_b, mpz_t a, mpz_t b);

bool get_bit(mpz_t x, int i);

void mpnc_shift(mp_ptr rp, mp_size_t rs, int offset);

void modular_inverse(mpz_t x, mpz_t a, mpz_t m);

#endif
