#ifndef NAIVE_H
#define NAIVE_H

#include "gmp/gmp.h"

/**
 * Computes ret = (x^a)*(y^b) mod N
 * Does it the naive way : computes sequentially x1 = x^a mod N, then x2 = y^b mod N and finally does ret = (x1 * x2) mod N.
 *
 * @param ret Return holder parameter
 * @param x   First radix
 * @param y   Second radix
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 */
void naive_multiexp(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N);

#endif
