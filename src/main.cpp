#include <iostream>
#include "gmp/gmp.h"
#include "gmp/gmp-impl.h"
#include <chrono>
#include <cmath>

#include "utils.hpp"
#include "lenstra_montgomery.hpp"
#include "montgomery.hpp"
#include "naive.hpp"
#include "square_multiply.hpp"
#include "square_multiply_montgomery.hpp"
#include "lenstra.hpp"
#include "benchmark.hpp"


mpz_t x, y, a, b, N, R, gcd, xm, ym, res, tmp, tmp2, mu, R_and, R_mod, naive;


mp_ptr rp, xp, xmp, yp, ymp, np, tmpp, tmp2p, onep, ret_tmp_p;
mp_limb_t np0;
mp_size_t xs, ys, xms, yms, ns, rs;
int lambda;
int k;
mpz_t R_prime;
mpz_t N_prime;
mpz_t one, modulus, trash, Q;


mpz_t beta_n, beta_n2, beta_n2_and, mu2, n_prime2;


int main(int argc, char *argv[]) {
        lambda = std::atoi(argv[1]);
        k = std::atoi(argv[2]);
        int count = std::atoi(argv[3]);

        gmp_randstate_t rstate;
        gmp_randinit_mt(rstate);


        mpz_init2(tmp, 3*lambda);
        mpz_init2(tmp2, 3*lambda);

        tmpp = PTR(tmp);
        tmp2p = PTR(tmp2);

        mpz_init(one);
        mpz_set_ui(one, 1);
        onep = PTR(one);

        mpz_init(modulus);
        mpz_setbit(modulus, 64);

        mpz_init(trash);

        mpz_init(Q);
        mpz_init(mu);

        for(int i = 0; i<count; i++)
        {
                mpz_rrandomb(x, rstate, lambda-2);
                mpz_rrandomb(y, rstate, lambda-2);
                mpz_rrandomb(a, rstate, k);
                mpz_rrandomb(b, rstate, k);
                mpz_rrandomb(N, rstate, lambda-3);
                mpz_mul_ui(N, N, 2);
                mpz_add_ui(N, N, 1);

                mpz_init(gcd);
                mpz_init(R);
                mpz_init(R_prime);
                mpz_init(N_prime);

                mpz_setbit(R, lambda);
                mpz_sub_ui(R_and, R, 1);

                mpz_init(R_mod);
                mpz_mod(R_mod, R, N);

                extended_euclide(gcd, R_prime, N_prime, R, N);
                mpz_neg(N_prime, N_prime);


                mpz_mod(trash, N_prime, modulus);
                np0 = PTR(trash)[0];


                mpz_init2(xm, 2*lambda);
                mpz_init2(ym, 2*lambda);

                rp = PTR(res);
                xp = PTR(x);
                yp = PTR(y);
                xmp = PTR(xm);
                ymp = PTR(ym);
                np = PTR(N);

                xs = SIZ(x);
                ys = SIZ(y);
                ns = SIZ(N);

                redcify (xmp, xp, xs, np, ns);
                SIZ(xm) = ns;
                redcify (ymp, yp, ys, np, ns);
                SIZ(ym) = ns;


                mpz_t rt;
                l mpz_init2(rt, 2*lambda);
                mp_ptr rtp = PTR(rt);


                mpz_init(beta_n2);
                mpz_init(beta_n2_and);
                mpz_init(mu2);
                mpz_init(n_prime2);

                mpz_setbit(beta_n2, lambda/2);

                mpz_sub_ui(beta_n2_and, beta_n2, 1);

                mpz_t gcd_trash, R_prime_trash;
                mpz_init(gcd_trash);
                mpz_init(R_prime_trash);
                extended_euclide(gcd_trash, R_prime_trash, mu2, R, N);
                mpz_neg(mu2, mu2);
                mpz_and(mu2, mu2, beta_n2_and);

                mpz_mul(n_prime2, mu2, N);





                /*
                   std::cout << "x = " << x << std::endl;
                   std::cout << "y = " << y << std::endl;
                   std::cout << "n = " << N << std::endl;
                   std::cout << "r = " << R << std::endl;
                   std::cout << "r_prime = " << R_prime << std::endl;
                   std::cout << "n_prime = " << N_prime << std::endl;
                   std::cout << "np0 = " << np0 << std::endl;
                   std::cout << "xm = " << xm << std::endl;
                   std::cout << "ym = " << ym << std::endl;


                   std::cout << "exp = " << expected << std::endl;
                   std::cout << "tmp = " << rt << std::endl;
                   std::cout << "res = " << res << std::endl;

                   std::cout << "xm*ym * r_prime % n == tmp" << std::endl;
                 */


                mpz_init2(naive, 2*lambda);
                naive_multiexp(naive, x, y, a, b, N);

                WindowBenchmark naive_computation (naive_multiexp, x, y, a, b, N, 2);

                WindowBenchmark sq_vanilla (square_multiply, x, y, a, b, N, 2);
                WindowBenchmark sq_montgomery (square_multiply_montgomery, x, y, a, b, N, 2);

                DirectBenchmark lenstra_2 (lenstra_exponentiation, lenstra_precomputations, x, y, a, b, N, 2);
                DirectBenchmark lenstra_montgomery_2 (lenstra_montgomery_exponentiation, lenstra_montgomery_precomputations, x, y, a, b, N, 2);


                DirectBenchmark lenstra_3 (lenstra_exponentiation, lenstra_precomputations, x, y, a, b, N, 3);
                DirectBenchmark lenstra_montgomery_3 (lenstra_montgomery_exponentiation, lenstra_montgomery_precomputations, x, y, a, b, N, 3);

                std::cout << "Naive" << std::endl;
                naive_computation.time();
                std::cout << std::endl;
                std::cout << std::endl;

                std::cout << "S&Q normal" << std::endl;
                sq_vanilla.time();
                std::cout << std::endl;
                std::cout << std::endl;

                std::cout << "S&Q Montgomery" << std::endl;
                sq_montgomery.time();
                std::cout << std::endl;
                std::cout << std::endl;

                std::cout << "Lenstra 2" << std::endl;
                lenstra_2.time();
                std::cout << std::endl;
                std::cout << std::endl;

                std::cout << "Lenstra 2 Montgomery" << std::endl;
                lenstra_montgomery_2.time();
                std::cout << std::endl;
                std::cout << std::endl;

                std::cout << "Lenstra 3" << std::endl;
                lenstra_3.time();
                std::cout << std::endl;
                std::cout << std::endl;

                std::cout << "Lenstra 3 Montgomery" << std::endl;
                lenstra_montgomery_3.time();
                std::cout << std::endl;

                std::cout << 0 << std::endl;

        }
        return 0;
}
