#ifndef W_ARRY_H
#define W_ARRY_H

#include "gmp/gmp.h"

void w_arry_exponentiation(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, mpz_t * pc);


void w_arry_precomputations(mpz_t* pc, int pow_w, mpz_t x, mpz_t y, mpz_t N);
#endif
