#include "square_multiply_montgomery.hpp"
#include "gmp/gmp-impl.h"
#include "montgomery.hpp"

#include <iostream>


extern int k, lambda;
extern mpz_t one, R, N;


void powm_montgomery(mpz_t ret, mpz_t x, mpz_t a)
{
  mpz_mod(ret, R, N);
  for(int i = k; i >= 0; i--)
  {
    montgomery_mul(ret, ret, ret);
    if(mpz_tstbit(a, i))
    {
      montgomery_mul(ret, ret, x);
    }
  }
}

void square_multiply_montgomery(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N)
{
        mpz_t tmp, xm, ym;
        mpz_init(tmp);
        mpz_init2(xm, 2*lambda);
        mpz_init2(ym, 2*lambda);

        redcify (PTR(xm), PTR(x), SIZ(x), PTR(N), SIZ(N));
        redcify (PTR(ym), PTR(y), SIZ(y), PTR(N), SIZ(N));

        SIZ(xm) = SIZ(N);
        SIZ(ym) = SIZ(N);


        powm_montgomery(tmp, xm, a);
        powm_montgomery(ret, ym, b);
        montgomery_mul(ret, tmp, ret);
        montgomery_mul(ret, ret, one);
}
