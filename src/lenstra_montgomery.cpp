#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "lenstra_montgomery.hpp"
#include "utils.hpp"
#include "montgomery.hpp"
#include "gmp/gmp-impl.h"


extern mpz_t R, R_prime, xm, ym, N, one;
extern int lambda;

void lenstra_montgomery_precomputations(mpz_t* pc, int pow_w, mpz_t x, mpz_t y, mpz_t N)
{
        redcify (PTR(xm), PTR(x), SIZ(x), PTR(N), SIZ(N));
        redcify (PTR(ym), PTR(y), SIZ(y), PTR(N), SIZ(N));

        for(int i = 0; i<pow_w; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {
                        mpz_init2(pc[i * pow_w + j], 10*lambda);
                }
        }


        // Precomputation computations itself.
        mpz_set(pc[1 * pow_w + 0], xm);
        mpz_set(pc[0 * pow_w + 1], ym);
        //montgomery_mul(pc[2 * pow_w + 0], x_mon, x_mon);
        montgomery_mul(pc[2 * pow_w + 0], xm, xm);

        //montgomery_mul(pc[0*pow_w+2], y_mon, y_mon);
        montgomery_mul(pc[0*pow_w+2], ym, ym);

        int mid = pow_w/2;
        for(int i = 1; i < mid; i++)
        {
                //montgomery_mul(pc[(2*i+1) * pow_w + 0], pc[(2*(i-1)+1) * pow_w + 0], pc[2 * pow_w + 0]);
                montgomery_mul(pc[(2*i+1) * pow_w + 0], pc[(2*(i-1)+1) * pow_w + 0], pc[2 * pow_w + 0]);

                //montgomery_mul(pc[0 * pow_w + 2*i+1], pc[0 * pow_w + 2*(i-1)+1], pc[0 * pow_w + 2]);
                montgomery_mul(pc[0 * pow_w + 2*i+1], pc[0 * pow_w + 2*(i-1)+1], pc[0 * pow_w + 2]);
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < pow_w; j++)
                {
                        //  montgomery_mul(pc[j * pow_w + 2*i+1], pc[(j-1) * pow_w + 2*i+1], x_mon);
                        montgomery_mul(pc[j * pow_w + 2*i+1], pc[(j-1) * pow_w + 2*i+1], xm);
                }
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < mid; j++)
                {
                        //montgomery_mul(pc[(2*i+1) * pow_w + 2*j], pc[(2*i+1) * pow_w + 2*j-1],y_mon);
                        montgomery_mul(pc[(2*i+1) * pow_w + 2*j], pc[(2*i+1) * pow_w + 2*j-1],ym);
                }
        }
}


/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */
void lenstra_montgomery_exponentiation(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, mpz_t* pc)
{
        int pow_w = pow(2, w);

        mpz_mod(ret, R, N);
        int j = k-1;

        while(j>=0)
        {
                if(simultaneous_zero_bit(a, b, j))
                {
                        //montgomery_mul(ret, ret, ret);
                        montgomery_mul(ret, ret, ret);
                        j--;
                }
                else
                {
                        int j_new = std::max(j-w, -1);
                        int J = j_new+ 1;

                        while(simultaneous_zero_bit(a,b, J) == true)
                        {
                                J++;
                        }
                        int A = filter(a, j, J);
                        int B = filter(b, j, J);

                        while(j>=J)
                        {
                                //montgomery_mul(ret, ret, ret);
                                montgomery_mul(ret, ret, ret);
                                j--;
                        }

                        //montgomery_mul(ret, ret, pc[A * pow_w + B]);
                        //montgomery_mul(ret, ret, pc[A * pow_w + B]);
                        montgomery_mul(ret, ret, pc[A * pow_w + B]);



                        while(j>j_new)
                        {
                                //montgomery_mul(ret, ret, ret);
                                //montgomery_mul(ret, ret, ret);
                                montgomery_mul(ret, ret, ret);
                                j--;
                        }

                }
        }

        //montgomery_to_mpz(ret, ret);
        //montgomery_mul(ret, ret, one);
        //
        montgomery_mul(ret, ret, one);
}
