#include "square_multiply.hpp"
#include "gmp/gmp-impl.h"
#include "montgomery.hpp"

#include <iostream>


extern int k, lambda;
extern mpz_t one, R, N;


void powm(mpz_t ret, mpz_t x, mpz_t a)
{
  mpz_set_ui(ret, 1);
  for(int i = k; i >= 0; i--)
  {
    mpz_mul(ret, ret, ret);
    mpz_mod(ret, ret, N);
    if(mpz_tstbit(a, i))
    {
      mpz_mul(ret, ret, x);
      mpz_mod(ret, ret, N);
    }
  }
}

void square_multiply(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N)
{
        mpz_t tmp, xm, ym;
        mpz_init(tmp);

        powm(tmp, x, a);
        powm(ret, y, b);
        mpz_mul(ret, tmp, ret);
        mpz_mod(ret, ret, N);
}
