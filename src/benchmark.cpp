#include "benchmark.hpp"

#include <chrono>
#include <iostream>
#include <cmath>


/**
 * Takes a multiexponentiation algorithm f in parameter and computes ret = (x^a) * (x^b) mod N using f and prints out the time taken to compute.
 * @method benchmark
 * @param  f         Multiexponentiation algorithm
 * @param  ret       Return variable holder
 * @param  x
 * @param  y
 * @param  a
 * @param  b
 * @param  N         Modulus
 * @param  end       If true then prints a final ";". Should be used only if it is not the last benchmark so the line in a CSV file does not end by a ";" and causes bugs when parsing.
 */

extern int lambda, k;
extern mpz_t naive;


void benchmark (vFunctionCall f, mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, bool end)
{
        std::chrono::duration<double> time;

        auto start = std::chrono::high_resolution_clock::now();

        f(ret, x, y, a, b, N);


        auto finish = std::chrono::high_resolution_clock::now();

        time = finish - start;


        std::cout << time.count();

        if(end == 1)
                std::cout << ";";
}



/**
 * Takes a multiexponentiation algorithm f of the Lenstra family in parameter and computes ret = (x^a) * (x^b) mod N using f and prints out the time taken to compute.
 * @method benchmark
 * @param  f         Multiexponentiation algorithm
 * @param  ret       Return variable holder
 * @param  x
 * @param  y
 * @param  a
 * @param  b
 * @param  N         Modulus
 * @param  k         Max exponent bitlength
 * @param  w         Lenstra window size
 * @param  end       If true then prints a final ";". Should be used only if it is not the last benchmark so the line in a CSV file does not end by a ";" and causes bugs when parsing.
 */
void benchmark (vWindowCall f, vWindowPrecomput p, mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, bool end)
{
        std::chrono::duration<double> time;
        std::chrono::duration<double> time_pc;

        auto start = std::chrono::high_resolution_clock::now();
        int pow_w = pow(2, w);
        mpz_t pc[pow_w][pow_w];

        p((mpz_t *)pc, pow_w, x, y, N);

        auto step = std::chrono::high_resolution_clock::now();
        time_pc = step - start;
        std::cout << time_pc.count() << ";";

        f(ret, x, y, a, b, N, k, w, (mpz_t *)pc);


        auto finish = std::chrono::high_resolution_clock::now();
        time = finish - start;

        std::cout << time.count();


        if(end == 1)
                std::cout << ";";
}

DirectBenchmark::DirectBenchmark(vWindowCall f, vWindowPrecomput p, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int w)
{
  auto start = std::chrono::high_resolution_clock::now();


  int pow_w = pow(2, w);
  mpz_t pc[pow_w][pow_w];

  p((mpz_t *)pc, pow_w, x, y, N);


  mpz_t ret;
  mpz_init2(ret, 2*lambda);

  auto step = std::chrono::high_resolution_clock::now();
  precomputation_time = step - start;


  f(ret, x, y, a, b, N, k, w, (mpz_t *)pc);


  auto finish = std::chrono::high_resolution_clock::now();
  computation_time = finish - start;

  assert(mpz_cmp(naive, ret) == 0);
}

void DirectBenchmark::time()
{
  std::cout << precomputation_time.count() << ";" << computation_time.count() << ";";
}



WindowBenchmark::WindowBenchmark(vFunctionCall f, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int w)
{
  mpz_t ret;
  mpz_init2(ret, 2*lambda);
  auto start = std::chrono::high_resolution_clock::now();

  f(ret, x, y, a, b, N);


  auto finish = std::chrono::high_resolution_clock::now();

  computation_time = finish - start;
  assert(mpz_cmp(naive, ret) == 0);
}

void WindowBenchmark::time()
{
  std::cout << computation_time.count() << ";";
}
