#include <iostream>
#include <math.h>       /* floor */

#include "w-arry.hpp"
#include "utils.hpp"

void w_arry_exponentiation(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, mpz_t * pc)
{
        int pow_w = pow(2, w);
        mpz_set_ui(ret, 1);

        for(int j = floor((k-1)/w)*w; j >= 0; j-=w)
        {
                for(int n = 1; n <= w; n++)
                {
                        mpz_mul(ret, ret, ret);
                        mpz_mod(ret, ret, N);
                }
                int A = filter(a, j+w-1, j);
                int B = filter(b, j+w-1, j);
                if(A != 0 || B != 0)
                {
                        mpz_mul(ret, ret, pc[A*pow_w + B]);
                        mpz_mod(ret, ret, N);
                }
        }
}


void w_arry_precomputations(mpz_t* pc, int pow_w, mpz_t x, mpz_t y, mpz_t N)
{
        for(int i = 0; i<pow_w; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {
                        mpz_init(pc[i * pow_w + j]);
                }
        }


        // Precomputation computations itself.
        for(int i = 0; i < pow_w; i++)
        {
                int j;
                if(i==0)
                {
                        mpz_set_ui(pc[0], 1);
                        mpz_set(pc[1], y);
                        j = 2;
                }
                else
                {
                        j = 1;
                        if(i == 1)
                        {
                                mpz_set(pc[pow_w], x);
                        }
                        else
                        {
                                mpz_mul(pc[i*pow_w], pc[(i-1)*pow_w], x);
                                mpz_mod(pc[i*pow_w], pc[i*pow_w], N);
                        }
                }

                for(; j<pow_w; j++)
                {
                        mpz_mul(pc[i * pow_w + j], pc[i * pow_w + j - 1], y);
                        mpz_mod(pc[i * pow_w + j], pc[i * pow_w + j], N);
                }
        }

        /*
           for(int i = 0; i< pow_w; i++)
           {
                for(int j = 0; j < pow_w; j++)
                {
                        std::cout << pc[i*pow_w + j] << "\t";
                }
                std::cout << std::endl;
           }
         */
}
