#ifndef MONTGOMERY_H
#define MONTGOMERY_H

#include "gmp.h"

void montgomery_mul(mpz_t r, mpz_t x, mpz_t y);
void fast_REDC(mpz_t r, mpz_t c);
void redcify (mp_ptr rp, mp_srcptr up, mp_size_t un, mp_srcptr mp, mp_size_t n);
int mpn_word_level_montgomery_mul(mp_ptr rp, mp_ptr ap, mp_size_t as, mp_ptr bp, mp_size_t bs);
int mpn_bit_level_montgomery_mul(mp_ptr rp, mp_ptr xp, mp_size_t xs);
void REDCSvoboda(mpz_t r, mpz_t c);
#endif
