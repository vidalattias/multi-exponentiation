#ifndef SQUARE_MULTIPLY_MONTGOMERY
#define SQUARE_MULTIPLY_MONTGOMERY

#include "gmp.h"

void square_multiply_montgomery(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N);

#endif
