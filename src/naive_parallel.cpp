#include <thread>
#include <pthread.h>
#include <mutex>

#include "naive_parallel.hpp"


/**
 * Computes ret = (x^a) mod N
 * It is used as an auxiliary function when calling a new thread.
 *
 * @param ret   Return value holder
 * @param radix
 * @param exp
 * @param N     Modulus
 */
void exponentiation(mpz_t ret, mpz_t radix, mpz_t exp, mpz_t N)
{
        mpz_powm(ret, radix, exp, N);
}



/**
 * Computes x1 = (x^a) mod N and operates ret = ret * x1 mod N and uses the mutex mtx to ensure integrity of the computation
 * It is used as an auxiliary function when calling a new thread.
 *
 * @param ret   Return value holder
 * @param radix
 * @param exp
 * @param N     Modulus
 * @param mtx   Mutex used during the final product
 */

void exponentiation_opti(mpz_t ret, mpz_t radix, mpz_t exp, mpz_t N, std::mutex* mtx)
{
        mpz_t tmp;
        mpz_init(tmp);
        mpz_powm(tmp, radix, exp, N);

        mtx->lock();
        mpz_mul(ret, ret, tmp);
        mpz_mod(ret, ret, N);
        mtx->unlock();
}



/**
 * Computes ret = (x^a)*(y^b) mod N
 * Computes in parallel x1 = x^a mod N and x2 = y^b mod N, then it waits for join of the threads and finally does ret = (x1 * x2) mod N.
 *
 * @param ret Return holder parameter
 * @param x   First radix
 * @param y   Second radix
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 */
void naive_parallel_multiexp(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N)
{
        mpz_t x1, x2;
        mpz_init(x1);
        mpz_init(x2);


        std::thread first(exponentiation, x1, x, a, N);
        std::thread second(exponentiation, x2, y, b, N);

        first.join();
        second.join();

        mpz_mul(ret, x1, x2);
        mpz_mod(ret, ret, N);
}



/**
 * Computes ret = (x^a)*(y^b) mod N
 * Slightly optimized from naive_parallel_multiexp. Instead of computing the final product after joining the two threads, each thread does the product directly.
 *
 * @param ret Return holder parameter
 * @param x   First radix
 * @param y   Second radix
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 */
void naive_parallel_multiexp_opti(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N)
{
        std::mutex mtx;
        mpz_set_ui(ret, 1);
        std::thread first(exponentiation_opti, ret, x, a, N, &mtx);
        std::thread second(exponentiation_opti, ret, y, b, N, &mtx);

        first.join();
        second.join();
}
