#ifndef SQUARE_MULTIPLY
#define SQUARE_MULTIPLY

#include "gmp.h"

void square_multiply(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N);

#endif
