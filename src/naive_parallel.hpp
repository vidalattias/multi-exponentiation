#ifndef NAIVE_PARALLEL_H
#define NAIVE_PARALLEL_H

#include "gmp/gmp.h"

#include "naive_parallel.hpp"

/**
 * Computes ret = (x^a)*(y^b) mod N
 * Computes in parallel x1 = x^a mod N and x2 = y^b mod N, then it waits for join of the threads and finally does ret = (x1 * x2) mod N.
 *
 * @param ret Return holder parameter
 * @param x   First radix
 * @param y   Second radix
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 */
void naive_parallel_multiexp(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N);



/**
 * Computes ret = (x^a)*(y^b) mod N
 * Slightly optimized from naive_parallel_multiexp. Instead of computing the final product after joining the two threads, each thread does the product directly.
 *
 * @param ret Return holder parameter
 * @param x   First radix
 * @param y   Second radix
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 */

void naive_parallel_multiexp_opti(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N);

#endif
