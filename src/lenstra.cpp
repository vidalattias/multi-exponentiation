#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#include "lenstra.hpp"
#include "utils.hpp"

void lenstra_precomputations(mpz_t* pc, int pow_w, mpz_t x, mpz_t y, mpz_t N)
{
        for(int i = 0; i<pow_w; i++)
        {
                for(int j = 0; j < pow_w; j++)
                {
                        mpz_init(pc[i * pow_w + j]);
                }
        }


        // Precomputation computations itself.
        mpz_set(pc[1 * pow_w + 0], x);
        mpz_set(pc[0 * pow_w + 1], y);
        mpz_mul(pc[2 * pow_w + 0], x, x);
        mpz_mod(pc[2 * pow_w + 0], pc[2 * pow_w + 0], N);
        mpz_mul(pc[0 * pow_w + 2], y, y);
        mpz_mod(pc[0 * pow_w + 2], pc[0 * pow_w + 2], N);

        int mid = pow_w/2;
        for(int i = 1; i < mid; i++)
        {
                mpz_mul(pc[(2*i+1) * pow_w + 0], pc[(2*(i-1)+1) * pow_w + 0], pc[2 * pow_w + 0]);
                mpz_mod(pc[(2*i+1) * pow_w + 0], pc[(2*i+1) * pow_w + 0], N);

                mpz_mul(pc[0 * pow_w + 2*i+1], pc[0 * pow_w + 2*(i-1)+1], pc[0 * pow_w + 2]);
                mpz_mod(pc[0 * pow_w + 2*i+1], pc[0 * pow_w + 2*i+1], N);
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < pow_w; j++)
                {
                        mpz_mul(pc[j * pow_w + 2*i+1], pc[(j-1) * pow_w + 2*i+1], x);
                        mpz_mod(pc[j * pow_w + 2*i+1], pc[j * pow_w + 2*i+1], N);
                }
        }

        for(int i = 0; i < mid; i++)
        {
                for(int j = 1; j < mid; j++)
                {
                        mpz_mul(pc[(2*i+1) * pow_w + 2*j], pc[(2*i+1) * pow_w + 2*j-1],y);
                        mpz_mod(pc[(2*i+1) * pow_w + 2*j], pc[(2*i+1) * pow_w + 2*j], N);
                }
        }
}


/**
 * Implementation of the Lenstra algorithm for modular multiexponentiation. Computes ret = (x^a)*(y^b) mod N.
 *
 *
 * @param ret Variable in which to write the result of the multiexponentiation.
 * @param x   Radix of first exponentiation
 * @param y   Radix of second exponentiation
 * @param a   First exponent
 * @param b   Second exponent
 * @param N   Modulus
 * @param k   Max bitlength of a and b
 * @param w   Lenstra's window size parameter. See the paper for more informations
 */
void lenstra_exponentiation(mpz_t ret, mpz_t x, mpz_t y, mpz_t a, mpz_t b, mpz_t N, int k, int w, mpz_t* pc)
{
        int pow_w = pow(2, w);


        mpz_set_ui(ret, 1);
        int j = k-1;

        while(j>=0)
        {
                if(simultaneous_zero_bit(a, b, j))
                {
                        mpz_mul(ret, ret, ret);
                        mpz_mod(ret, ret, N);
                        j--;
                }
                else
                {
                        int j_new = std::max(j-w, -1);
                        int J = j_new+ 1;

                        while(simultaneous_zero_bit(a,b, J) == true)
                        {
                                J++;
                        }
                        int A = filter(a, j, J);
                        int B = filter(b, j, J);

                        while(j>=J)
                        {
                                mpz_mul(ret, ret, ret);
                                mpz_mod(ret, ret, N);
                                j--;
                        }

                        mpz_mul(ret, ret, pc[A * pow_w + B]);
                        mpz_mod(ret, ret, N);

                        while(j>j_new)
                        {
                                mpz_mul(ret, ret, ret);
                                mpz_mod(ret, ret, N);
                                j--;
                        }

                }
        }
}
